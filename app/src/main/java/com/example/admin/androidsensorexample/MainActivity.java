package com.example.admin.androidsensorexample;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SensorEventListener {

    AppCompatButton light_sensor, proximity_sensor, magnetic_sensor, accelerometer_sensor, orientation_sensor;
    private SensorManager mSensorManager;
    private Sensor mSensor;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorInitialization();
        viewInitialization();
    }

    private void viewInitialization(){

        text = findViewById(R.id.text);
        light_sensor = findViewById(R.id.light_sensor);
        proximity_sensor = findViewById(R.id.proximity_sensor);
        magnetic_sensor = findViewById(R.id.magnetic_sensor);
        accelerometer_sensor = findViewById(R.id.accelerometer_sensor);
        orientation_sensor = findViewById(R.id.orientation_sensor);

        light_sensor.setOnClickListener(this);
        proximity_sensor.setOnClickListener(this);
        magnetic_sensor.setOnClickListener(this);
        accelerometer_sensor.setOnClickListener(this);
        orientation_sensor.setOnClickListener(this);
    }

    private void sensorInitialization() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        Log.d("DeviceSensor", "Sensor List Size " + deviceSensors.size());
        printAvailableSensors(deviceSensors);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.light_sensor:
                mSensorManager.unregisterListener(this);
                mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
                mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
                Toast.makeText(this, "Light Sensor", Toast.LENGTH_SHORT).show();
                break;

            case R.id.proximity_sensor:
                mSensorManager.unregisterListener(this);
                mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
                mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
                Toast.makeText(this, "Proximity Sensor", Toast.LENGTH_SHORT).show();
                break;

            case R.id.magnetic_sensor:
                mSensorManager.unregisterListener(this);
                mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
                mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
                Toast.makeText(this, "Magnetic Sensor", Toast.LENGTH_SHORT).show();
                break;

            case R.id.accelerometer_sensor:
                mSensorManager.unregisterListener(this);
                mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
                mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
                Toast.makeText(this, "Accelerometer Sensor", Toast.LENGTH_SHORT).show();
                break;

            case R.id.orientation_sensor:
                mSensorManager.unregisterListener(this);
                mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
                mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
                Toast.makeText(this, "Orientation Sensor", Toast.LENGTH_SHORT).show();
                break;

        }
    }

    private void printAvailableSensors(List<Sensor> deviceSensors) {
        for (int i = 0; i < deviceSensors.size(); i++) {
            Log.d("DeviceSensor", deviceSensors.get(i).getName() + " " + deviceSensors.get(i).getVendor());
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        switch (sensorEvent.sensor.getType()){
            case Sensor.TYPE_LIGHT:
                text.setText(sensorEvent.sensor.getName()+"\n"+String.valueOf(sensorEvent.values[0]));
                break;
            case Sensor.TYPE_PROXIMITY:
                text.setText(sensorEvent.sensor.getName()+"\n"+String.valueOf(sensorEvent.values[0]));
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:

                double angle = (Math.atan2(sensorEvent.values[1]/10, sensorEvent.values[0]/10) * 180) / Math.PI;
                text.setText(sensorEvent.sensor.getName()+"\nX: "+String.valueOf(sensorEvent.values[0])+"\nY: "+String.valueOf(sensorEvent.values[1])+"\nZ: "+String.valueOf(sensorEvent.values[2])
                +"\n"+angle);
                getDirection(angle);

                break;
            case Sensor.TYPE_ACCELEROMETER:
                text.setText(sensorEvent.sensor.getName()+"\nX: "+String.valueOf(sensorEvent.values[0])+"\nY: "+String.valueOf(sensorEvent.values[1])+"\nZ: "+String.valueOf(sensorEvent.values[2]));
                break;
            case Sensor.TYPE_ORIENTATION:
                text.setText(sensorEvent.sensor.getName()+"\nX: "+String.valueOf(sensorEvent.values[0])+"\nY: "+String.valueOf(sensorEvent.values[1])+"\nZ: "+String.valueOf(sensorEvent.values[2]));
                break;
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private void getDirection(double angle){

        if(angle>337.25 || angle<22.5){
            String tex = text.getText()+"\nAngle: North";
            text.setText(tex);
        }
        else if(angle>292.5 && angle<337.25){
            String tex = text.getText()+"\nAngle: North-West";
            text.setText(tex);
        }
        else if(angle>247.5 && angle<292.5){
            String tex = text.getText()+"\nAngle: South-West";
            text.setText(tex);
        }
        else if(angle>202.5 && angle<247.5){
            String tex = text.getText()+"\nAngle: South";
            text.setText(tex);
        }
        else if(angle>157.5 && angle<202.5){
            String tex = text.getText()+"\nAngle: South-East";
            text.setText(tex);
        }
        else if(angle>112.5 && angle<157.5){
            String tex = text.getText()+"\nAngle: East";
            text.setText(tex);
        }
        else if(angle>67.5 && angle<112.5){
            String tex = text.getText()+"\nAngle: North-East";
            text.setText(tex);
        }
        else if(angle>0 && angle<67.5){
            String tex = text.getText()+"\nAngle: North-East";
            text.setText(tex);
        }

    }


}
